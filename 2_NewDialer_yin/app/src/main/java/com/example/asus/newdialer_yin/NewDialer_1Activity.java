/**指定了一个intent-filter，Intent Filter(过滤器)用来匹配隐式Intent*/
package com.example.asus.newdialer_yin;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
/**加入EditText、PhoneNumberUtils和Toast的包*/
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NewDialer_1Activity extends AppCompatActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newdailer1);

        /**通过findViewById()方法获得EditText和Button对象的引用*/
        final EditText phoneNum=(EditText)findViewById(R.id.phoneNum);
        final Button button=(Button)findViewById(R.id.Button01);

        button.setOnClickListener(new Button.OnClickListener(){
            @SuppressLint("MissingPermission")
            public void onClick(View b){
                //Intent <Intent_name>=new Intent (<Action>,<Data>);
                /**通过.getText().toString()方法来获取输入的字符串，
                 * 然后进行验证一个EditText里输入的是不是纯数字
                 * 输入的电话号码应为数字，
                 * 但当在EditText里面输入的电话号码不是数字时，
                 * 此时输入错误信息时应出现相应的提示信息*/
                String call=phoneNum.getText().toString();

                /**判断电话号码的有效性可通过android.telephony.PhoneNumberUtils包中的isGlobalPhoneNumber()方法
                 * 当使用android.telephony.PhoneNumberUtils的isGlobalPhoneNumber()方法时，
                 * 在layout的string.xml里加入
                 * <string name="notify_incorrect_phonenum">您输入的号码不正确，请重新输</string>
                 * 以此来显示提示信息的内容*/
                if(PhoneNumberUtils.isGlobalPhoneNumber(call)){

                    /**使用intent实现拨打电话的功能，注意跟显式intent实现拨打电话功能的区别（见注释对比）*/
                    Intent I = new Intent(Intent.ACTION_CALL, Uri.parse("tel://" + call));
                    //Intent I=new Intent(Intent.ACTION_DIAL,Uri.parse(tel"://"+call));
                    startActivity(I);
                }
                /**当输入错误信息时*/
                else{
                    /**使用Toast类来管理信息的提示*/
                    Toast.makeText(NewDialer_1Activity.this,
                            R.string.notify_incorrect_phonenum, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
