/**
 * 1.点击一个按钮将信息广播出去；
 * 2.并定义接受的要求（不是谁都可以接收）；
 * 3.然后等待接受的文件被指定元素接受了，并显示反馈；
 * 4.显示广播信息,能在DDMS中查看广播信息。
 */
package com.example.asus.intentreceiver;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class IntentReceiverActivity extends AppCompatActivity {

    private Button myButton = null;
    /**设定Intent动作字符串用来标识唯一要广播的事件，因此，Intent里定义的字符串必须是独一无二的标识事件的字符串*/
    private final String nobody ="Who.do.you.think.you.are";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intentreceiver);
        myButton = (Button)findViewById(R.id.myButton);
        //为按钮对象设置监听器对象
        myButton.setOnClickListener(new MyButtonListener());
    }
    class MyButtonListener implements OnClickListener{
        @Override
        public void onClick(View v) {
            //生成一个Intent对象
            Intent intent = new Intent(nobody);
            //在Intent对象当中添加一个键值对
            /**当我们发送内容时，利用extras来增加额外的本地类型值*/
            intent.putExtra("key", "11111111111");
            /**最后使用sendBroadcast方法发送intent包含的信息
             * 箭头“nobody”是发出去的字符串*/
            sendBroadcast(intent);
        }
    }
}

