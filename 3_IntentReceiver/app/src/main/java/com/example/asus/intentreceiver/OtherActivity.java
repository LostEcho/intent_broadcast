/**接收broadcast需要：
 1.注册一个BroadcastReceiver
 2.并且要注册一个Intent Filter来制定BroadcaseReceiver是对哪些Intent进行监听。
 */
package com.example.asus.intentreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**创建一个新的Broadcast Receiver，
 * 需要扩展BroadcastReceiver类，
 * 并重写onReceive事件处理函数，
 * 将包含消息的Intent对象传给它。*/
public class OtherActivity extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        /**从Intent当中根据key取得value值*/
        String value = intent.getStringExtra("key");
        /**在Logcat中显示广播的信息*/
        Log.e("IntentReceiver-->Test", value);

        /**onReceive中代码的执行时间不要超过10s，否则android会弹出超时的dialog。*/
    }
}
