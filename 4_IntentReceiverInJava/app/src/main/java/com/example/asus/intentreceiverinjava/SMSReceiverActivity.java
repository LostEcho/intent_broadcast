package com.example.asus.intentreceiverinjava;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;


/**创建一个新的Broadcast Receiver，
 * 需要扩展BroadcastReceiver类，
 * 并重写onReceive事件处理函数，
 * 将包含消息的Intent对象传给它()*/

public class SMSReceiverActivity extends BroadcastReceiver{

    private static final String TAG = "onReceive" ;
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        Log.i(TAG, "onReceive-->running!");

        /**接收由Intent传来的数据*/
        Bundle bundle = intent.getExtras();

        /**30-37行为构建短信对象array,并依据收到的对象长度来建立array的大小*/

        //在Bundle对象当中有一个属性名为pdus，这个属性的值是一个Object数组
        /**pdus为 android内建短信参数， identifier 透过bundle.get("")并传一个包含pdus的对象*/
        Object[] myOBJpdus = (Object[]) bundle.get("pdus");

        //创建一个SmsMessage类型的数组
        SmsMessage[] messages = new SmsMessage[myOBJpdus.length];
        System.out.println(messages.length);
        for (int i = 0; i<myOBJpdus.length; i++)
        {
            //使用Object数组当中的对象创建SmsMessage对象
            messages[i] = SmsMessage.createFromPdu((byte[]) myOBJpdus[i]);

            /**调用SmsMessage对象的getDisppalyMessageBody()方法，可以得到消息的内容*/
            System.out.println(messages[i].getDisplayMessageBody());
        }
    }
}
