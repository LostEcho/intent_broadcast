package com.example.asus.newdialer_xian;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
/**加入Button的包和Uri的包*/
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NewDialerActivity extends AppCompatActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newdialer);

        Button dialer=(Button)findViewById(R.id.Button01);
        /**首先添加Button的事件响应*/
        dialer.setOnClickListener( new View.OnClickListener(){
            public void onClick(View v){
                //Intent <Intent_name>=new Intent (<Action>,<Data>);
                /**使用intent实现指定号码拨打电话的功能*/
                /**Intent <Intent name>=new Intent(<Action>,<Data>)
                 * Action：启动的activity做哪些动作，所触发动作的名字字符串
                 * data：描述intent要操作的数据URI和数据类型。本例是ACTION_DIAL数据类型必须为一个tel：//格式的电话114。
                 * Category：是对被请求组件的额外描述信息。
                 * Componnent name：启动哪一个activity
                 * Extra：在intent中附件额外信息，以便将数据传递给另外一个Activity
                 */
                Intent intent=new Intent(Intent.ACTION_DIAL,Uri.parse("tel://13192289342"));
                /**启动该Activity*/
                startActivity(intent);
            }
        });
    }
}
